# Actweb\Ipv4

This class gives you access to a number of common IPv4 functions :

checkValid($ip) - Returns true if the passed IP address was valid, will work with an IP address, IP/CIDR and IP/netmask

setRange($ip) - Takes an IP/CIDR or IP/Netmask and converts it into a range of addresses, this can then be used to
                check if an IP is within the range
                
inRange($ip) - Checks if the supplied IP address is within the range as set by 'setRange'

netmaskToCidr($mask) - Converts the passed netmask to CIDR notation

cidrToNetmask($cidr) - Converts the passed CIDR into a standard Netmask

isPrivate($ip) - Returns true if the supplied IP or IP range is within a private IP range, if any IPs fall outside the range, then it returns false.

getErrorString($error) - Returns the error string for the supplied error code

getLow - Return the lowest IP in the current range

getHigh - Return the highest IP in the current range

setLow($ip) - Allows setting of the lowest range IP

setHigh($ip) - Allows setting of the highest range IP

cidrCount($cidr) - Simply returns the number of IP addresses available for a selected CIDR bit mask

If there is an error then $class->error is set to the error number. and 'Null' is returned.
After calling 'checkValid' $class->ipAddr & $class-> cidr are set according to the passed IP

Error Codes :

BADIP   -> Invalid IP address

BADMASK -> Invalid Netmask or Bitmask
