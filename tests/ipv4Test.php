<?php
 /**
 * Short description for file
 *
 * Long description for file (if any)...
 * User: matt
 * Date: 13/04/15
 * Time: 02:30
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category   ACTweb/<CategoryName>
 * @package    ipv4
 * @author     Matt Lowe <marl.scot.1@googlemail.com>
 * @copyright  2015 ACTweb
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    0.0.1
 * @link       http://www.actweb.info/package/ipv4
 */


namespace Actweb;
include "../Ipv4.php";

class ipv4Test extends \PHPUnit_Framework_TestCase {
 public function setUp() { }
 public function tearDown() { }

 public function testCheckValid()
 {
  // test that function returns correct result
  $ipv4=new ipv4();
  $this->assertTrue($ipv4->checkValid('192.168.0.1'));
  $this->assertEquals('192.168.0.1', long2ip($ipv4->ipAddr));
  $this->assertTrue($ipv4->checkValid('192.168.0.1/30'));
  $this->assertEquals('192.168.0.1', long2ip($ipv4->ipAddr));
  $this->assertEquals('30', $ipv4->cidr);
  $this->assertTrue($ipv4->checkValid('192.168.0.1/255.255.255.0'));
  $this->assertTrue($ipv4->checkValid('192.168.0.1 255.255.0.0'));
  $this->assertTrue($ipv4->checkValid('10.250.10.0/255.255.0.0'));
  $this->assertEquals('10.250.10.0', long2ip($ipv4->ipAddr));
  $this->assertEquals('16', $ipv4->cidr);
  $this->assertFalse($ipv4->checkValid('hello world'));
  $this->assertFalse($ipv4->checkValid('265.192.32.23'));
  $this->assertFalse($ipv4->checkValid('192.168.0.1/33'));
  $this->assertFalse($ipv4->checkValid('192.168.0.1/255.255.255.255.5'));
  $this->assertFalse($ipv4->checkValid('192.168.0.1/-23.23.23.23'));
 }

 public function testNetmaskToCidr(){
  $ipv4=new ipv4();
  $this->assertEquals('32',$ipv4->netmaskToCidr('255.255.255.255'));
  $this->assertEquals('24',$ipv4->netmaskToCidr('255.255.255.0'));
  $this->assertEquals('0',$ipv4->netmaskToCidr('0.0.0.0'));

 }

 public function testCidrCount(){
  $ipv4=new ipv4();
  $this->assertEquals(256, $ipv4->cidrCount('24'));
  $this->assertEquals(1, $ipv4->cidrCount('32'));
 }

 public function testSetRange(){
  $ipv4=new ipv4();
  $ipv4->setRange('192.168.0.0/24');
  $this->assertEquals('192.168.0.0', $ipv4->getLow());
  $this->assertEquals('192.168.0.255', $ipv4->getHigh());
 }

 public function testInRange(){
  $ipv4=new ipv4();
  $ipv4->setRange('192.168.0.0/24');
  $this->assertTrue($ipv4->inRange('192.168.0.5'));
  $this->assertTrue($ipv4->inRange('192.168.0.0'));
  $this->assertTrue($ipv4->inRange('192.168.0.255'));
  $this->assertFalse($ipv4->inRange('192.169.0.5'));
  $this->assertFalse($ipv4->inRange('192.167.255.255'));
  $this->assertFalse($ipv4->inRange('192.169.0.0'));
 }

 public function testSetGet(){
  $ipv4=new ipv4();
  $ipv4->setLow('192.168.0.1');
  $ipv4->setHigh('192.168.0.255');
  $this->assertEquals('192.168.0.1', $ipv4->getLow());
  $this->assertEquals('192.168.0.255', $ipv4->getHigh());
 }

 public function testErrorCode(){
  $ipv4=new Ipv4();
  $ipv4->checkValid('192.168.0.1');
  $this->assertEquals(0, $ipv4->error);
  $ipv4->checkValid('192.168.0.256');
  $this->assertEquals(1, $ipv4->error);
  $this->assertEquals('Invalid IP address', $ipv4->getErrorString(1));
 }

 public function testIsPrivate(){
  $ipv4=new Ipv4();
  $this->assertTrue($ipv4->isPrivate('192.168.2.60'));
  $this->assertTrue($ipv4->isPrivate('10.250.43.83'));
  $this->assertTrue($ipv4->isPrivate('172.16.32.95'));
  $this->assertTrue($ipv4->isPrivate('192.168.2.60/24'));
  $this->assertTrue($ipv4->isPrivate('10.250.10.0/255.255.0.0'));
  $this->assertFalse($ipv4->isPrivate('192.153.153.178'));
  $this->assertFalse($ipv4->isPrivate('192.168.255.0/23'));
  $this->assertFalse($ipv4->isPrivate('192.168.255.0/6'));
 }

 public function testCidrToNetmask(){
  $ipv4= new Ipv4();
  $this->assertEquals('255.255.255.255', $ipv4->cidrToNetmask('32'));
  $this->assertEquals('255.255.255.0', $ipv4->cidrToNetmask('24'));
  $this->assertEquals('255.255.0.0', $ipv4->cidrToNetmask('16'));
  $this->assertEquals('255.255.255.248', $ipv4->cidrToNetmask('29'));
  $this->assertEquals('255.255.192.0', $ipv4->cidrToNetmask('18'));
 }
}
