<?php
/**
 * Set of utilities to verify IPv4 addresses
 *
 * And allow comparing addresses
 * works with single IP addresses and netmask/bitmask
 *
 * User: matt
 * Date: 11/04/15
 * Time: 13:45
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  ACTweb/ipv4
 * @package   Utils
 * @author    Matt Lowe <marl.scot.1@googlemail.com>
 * @copyright 2015 ACTweb
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   GIT: <git_id>
 * @link      http://www.actweb.info/package/ipv4
 */


namespace Actweb;

/**
 * IPv4 Validation and testing class
 *
 * Class ipv4
 * User: ${USER}
 * Date: ${DATE}
 * Time: ${TIME}
 *
 * @category  ACTweb/ipv4
 * @package   Ipv4
 * @author    Matt Lowe <marl.scot.1@googlemail.com>
 * @copyright 2015 ACTweb
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release: <package_version>
 * @link      http://www.actweb.info/package/ipv4
 */
class Ipv4
{
    //Constants
    /**
     * Error code for Bad IP
     * Error code for Bad Mask
     */
    const BADIP = 1;
    const BADMASK = 2;
    /**
     * If true, output logging to stdout
     *
     * @var bool
     */
    var $debug = false;
    /**
     * Error strings
     *
     * @var array
     */
    var $errorString
        = array(
            self::BADIP => 'Invalid IP address',
            self::BADMASK => 'Invalid net/bit mask',
        );

    /**
     * Temporary IP address used in local functions
     *
     * @var null
     */
    var $ipAddr = null;
    /**
     * Temporary CIDR for local functions
     *
     * @var null
     */
    var $cidr = null;
    /**
     * Holds error description
     *
     * @var string
     */
    var $error = '';
    /**
     * Holds error number
     *
     * @var int
     */
    var $errorNo = 0;
    /**
     * Lowest IP used when comparing IP address within range
     *
     * @var int
     */
    var $low = 0;
    /**
     * Highest IP used when comparing IP address within range
     *
     * @var int
     */
    var $high = 4294967295;

    /**
     * Basic Constructor
     */
    function __construct()
    {
        // dont do anything just now
    }

    /**
     * Checks if the supplied IPv4 address with optionable netmask
     * is valid.
     * IPv4 addresses can take the following formats
     * xxx.xxx.xxx.xxx                  Standard IPv4 Address
     * xxx.xxx.xxx.xxx/bb               IPv4 Address with cidr mask
     * xxx.xxx.xxx.xxx/mmm.mmm.mmm.mmm  Standard IPv4 Address and Netmask
     *
     * @param string $ipv4  IPv4 Address to check
     * @param bool   $noset When true, then This->ip/$this->cidr are not set
     *
     * @return bool Returns True on valid address
     */
    public function checkValid($ipv4, $noset = false)
    {
        $this->error = 0;
        if (!$noset) {
            $this->ipAddr = null;
            $this->cidr = null;
        }
        // ok trim any leading/trailing whitespace
        $ipv4 = trim($ipv4);
        // replace ' ' between ipv4 and netmask with '/'
        $ipv4 = str_replace(' ', '/', $ipv4);
        $split = strpos($ipv4, '/');
        if ($split > 0) {
            $mask = substr($ipv4, $split + 1);
            $ipv4 = substr($ipv4, 0, $split);
        } else {
            $mask = null;
        }
        $this->log($ipv4, $mask, $split);
        if (!filter_var($ipv4, FILTER_VALIDATE_IP)) {
            $this->error = self::BADIP;
            return false;
        }
        // Set temp IP var for other functions
        if (!$noset) {
            $this->ipAddr = ip2long($ipv4);
        }
        if (is_null($mask)) {
            // plain IPv4 so set cidr to 32
            if (!$noset) {
                $this->cidr = 32;
            }
            return true;
        }
        // ok lets check the net/cidr mask
        if (strlen($mask) > 2) {
            // ok this is more than 2 digits, so must be a netmask
            if (filter_var($mask, FILTER_VALIDATE_IP)) {
                if (!$noset) {
                    $this->cidr = $this->netmaskToCidr($mask);
                }
                return true;
            } else {
                $this->error = self::BADMASK;
                return false;
            }
        }
        // ok this is a cidr mask
        if (($mask < 0) || ($mask > 32)) {
            $this->error = self::BADMASK;
            return false;
        } else {
            if (!$noset) {
                $this->cidr = $mask;
            }
            return true;
        }
    }

    /**
     * Returns the $low var as an IPv4 address
     *
     * @return string
     */
    public function getLow()
    {
        return long2ip($this->low);
    }

    /**
     * Sets the $low var from an IPv4 address
     *
     * @param string $low IP address
     *
     * @return void
     */
    public function setLow($low)
    {
        $this->low = (ip2long($low));
    }

    /**
     * Returns the $high var as an IPv4 address
     *
     * @return string
     */
    public function getHigh()
    {
        return long2ip($this->high);
    }

    /**
     * Sets the $high var from an IPv4 address
     *
     * @param string $high IP address
     *
     * @return void
     */
    public function setHigh($high)
    {
        $this->high = ip2long($high);
    }

    /**
     * Gets the error string for the passed error code
     *
     * @param int $error Error number to fetch description for
     *
     * @return array
     */
    public function getErrorString($error)
    {
        return $this->errorString[$error];
    }

    /**
     * Checks to see if supplied IPv4 address is within
     * the set High/Low IP range
     *
     * @param string $ipv4 IPv4 address to check
     *
     * @return bool|null
     */
    public function inRange($ipv4)
    {
        if (!$this->checkValid($ipv4)) {
            return null;
        }
        $iplong = ip2long($ipv4);
        if (($iplong >= $this->low) && ($iplong <= $this->high)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Converts an IPv4 netmask into CIDR notation
     *
     * @param string $netmask IPv4 netmask
     *
     * @return int CIDR
     */
    public function netmaskToCidr($netmask)
    {
        if (!$this->checkValid($netmask, true)) {
            $this->error = self::BADMASK;
            return null;
        }
        $long = ip2long($netmask);
        $base = ip2long('255.255.255.255');
        return 32 - log(($long ^ $base) + 1, 2);
    }

    /**
     * Converts the supplied CIDR bitmask into
     * a standard 4 octect netmask
     *
     * @param int $cidr CIDR bit mask to convert
     *
     * @return string
     */
    public function cidrToNetmask($cidr)
    {
        if (($cidr<0) || ($cidr>32)) {
            $this->error=self::BADMASK;
            return null;
        }
        $mask = long2ip(-1 << (32 - (int)$cidr));
        return $mask;
    }

    /**
     * Returns the number of IPs within
     * the supplied CIDR
     *
     * @param string $cidr CIDR to count
     *
     * @return int Number of IPs
     */
    public function cidrCount($cidr)
    {
        if (($cidr<0) || ($cidr>32)) {
            $this->error=self::BADMASK;
            return null;
        }
        $number = 32 - $cidr;
        $number = pow(2, $number);
        return $number;
    }

    /**
     * Takes supplied IP/[CIDR/NETMASK] and sets
     * the low and high ranges.
     *
     * @param string $ipAddr IP address and cidr/mask to use
     *
     * @return bool True on sucsess
     */
    public function setRange($ipAddr)
    {
        /*
         * Check if valid and get the ip/cidr
         */
        if (!$this->checkValid($ipAddr)) {
            return null;
        }
        if (is_null($this->cidr)) {
            $this->cidr=32;
        }
        $this->low = $this->ipAddr;
        $this->high = $this->ipAddr;
        $this->high += $this->cidrCount($this->cidr) - 1;
        return true;
    }

    /**
     * Checks if debug is enabled and outputs
     * supplied values to std out
     *
     * @return void
     */
    public function log()
    {
        if ($this->debug) {
            foreach (func_get_args() as $value) {
                print_r($value);
                echo "\n";
            }
        }
    }

    /**
     * Returns true if the supplied IP address is in
     * one of the RFC1918 private IP ranges
     * returns NULL if there was a problem with the supplied IP
     * Will handle net/bit masks, but only checks first IP in range
     *
     * @param string $ipAddr IP address to check
     *
     * @return bool|null
     */
    public function isPrivate($ipAddr)
    {

        if (!$this->checkValid($ipAddr)) {
            return null;
        }
        $this->setRange($ipAddr);
        // check that the cidr is not lower than 8 (as private IPs cant be lower than this)
        if ($this->cidr < '8') {
            return false;
        }
        if (filter_var(
            $this->getLow(), FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE
        )) {
            // ok the lower ip is public
            return false;
        }
        if (filter_var(
            $this->getHigh(), FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE
        )) {
            // ok the higher ip is public
            return false;
        }
        return true;
    }
}